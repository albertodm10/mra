package tdd.training.mra;

import java.util.*;

public class MarsRover {

	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private int positionX;
	private int positionY;
	private char direction;
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if(planetX < 1 || planetY < 1) {
			throw new MarsRoverException("impossible to create empty planet");
		}
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		
		positionX = 0;
		positionY = 0;
		direction = 'N';
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(x<1 || y<1 || x>planetX || y>planetY) {
			throw new MarsRoverException("impossible to calculate whether planet contains obstacle");
		}
		String defaultString = "(" + x + "," + y + ")";
		
		for(int i=0; i<planetObstacles.size(); i++) {
			if(planetObstacles.get(i).equals(defaultString)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if(!commandString.contains("") && !commandString.contains("b") && !commandString.contains("f") && !commandString.contains("l") && !commandString.contains("r") ) {
			throw new MarsRoverException("Command not found");
		}
		if(!commandString.equals("")) {
			for (int i=0; i<commandString.length(); i++) {
				if(commandString.charAt(i) == 'l') {
					executeTurnLeftCommand();
				}
				else if(commandString.charAt(i) == 'r') {
					executeTurnRightCommand();		
				}
				else if(commandString.charAt(i) == 'f') {
					executeMoveForwardCommand();
				}
				else if(commandString.charAt(i) == 'b') {
					executeMoveBackwardCommand();
				}
			}
		}
		
		return "(" + positionX + "," + positionY + "," + direction + ")";
	}
	
	private void setPositionX(int x) {
		positionX = x;
	}
	
	private void setPositionY(int y) {
		positionY = y;
	}
	
	private void setDirection(char d) {
		direction = d;
	}
	
	public void setNewPositionAndDirection (int x, int y, char d) {
		setPositionX(x);
		setPositionY(y);
		setDirection(d);
	}

	private void executeTurnLeftCommand() {
		switch(direction) {
		case 'N': 
			direction = 'W';
		break;
		case 'S': 
			direction = 'E';
		break;
		case 'E': 
			direction = 'N';
		break;
		case 'W': 
			direction = 'S';
		break;
		}
	}
	
    private void executeTurnRightCommand() {
    	switch(direction) {
		case 'N': 
			direction = 'E';
		break;
		case 'S': 
			direction = 'W';
		break;
		case 'E': 
			direction = 'S';
		break;
		case 'W': 
			direction = 'N';
		break;
		}
	}
    
    private void executeMoveForwardCommand() {
    	switch(direction) {
		case 'N': 
			positionY = positionY + 1;
		break;
		case 'S': 
			positionY = positionY - 1;
		break;
		case 'E': 
			positionX = positionX + 1;
		break;
		case 'W': 
			positionX = positionX - 1;
		break;
		}
	}
    
    private void executeMoveBackwardCommand() {
    	switch(direction) {
		case 'N': 
			positionY = positionY - 1;
		break;
		case 'S': 
			positionY = positionY + 1;
		break;
		case 'E': 
			positionX = positionX - 1;
		break;
		case 'W': 
			positionX = positionX + 1;
		break;
		}
	}


}
