package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testCreateMarsRoverAndCheckObstacles() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(4, 7);
		
		assertTrue(obstacle);
	}
	
	@Test (expected = MarsRoverException.class)
	public void testCreateEmptyMarsRover() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(1, 0, planetObstacles);
	}
	
	@Test (expected = MarsRoverException.class)
	public void testWrongCheckingPlanetObstacles() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(4, 0);
	}
	
	@Test
	public void testCreateMarsRoverAndInitializeLandingPosition() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String positionAndDirectionRover = rover.executeCommand("");
		
		assertEquals("(0,0,N)", positionAndDirectionRover);
	}
	
	@Test
	public void testTurningLeftMarsRover() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void testTurningRightMarsRover() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void testMovingForwardMarsRover() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		rover.setNewPositionAndDirection(7, 6, 'N');
		
		assertEquals("(7,7,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void testMovingBackwardMarsRover() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		rover.setNewPositionAndDirection(5, 8, 'E');
		
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void testExecutionMultipleCommandsMarsRover() throws MarsRoverException{
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		rover.setNewPositionAndDirection(0, 0, 'N');
		
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}

}
